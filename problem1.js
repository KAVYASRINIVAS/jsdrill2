function checknum(n){
    if(n.length==0){
        return "Empty array!";
    }
    for(let i=0;i<n.length;i++){
        for(let j=0;j<n[i].length;j++){
            let c=n[i].charCodeAt(j)
            if(c!=36 && c!=44 && c!=45 && c!=46 && (c<48 || c>57)){
                return 0;
            }
            n[i]=n[i].replaceAll("$","");
            n[i]=n[i].replaceAll(",","");
        }
    
    }
    n=n.map(Number);
    //console.log(typeof(n[1]));
    //console.log(n[1]);
    return n;
}

module.exports.checkno=checknum;

/*
let t1=['$111.0','-$111.1'];
let t2=['$100.45', '$1,002.22', '-$123'];
let t3=['$ab11','-12345.6'];

console.log(checknum(t1));
console.log(checknum(t2));
console.log(checknum(t3));
*/