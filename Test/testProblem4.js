const testProb4=require('../problem4.js');

let t1={"first_name": "JoHN", "last_name": "SMith"};
let t2={"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
let t3={"first_name": "KaVyA", "last_name": "SRInIvAs"};

const test1=testProb4.title(t1);
console.log(test1);

const test2=testProb4.title(t2);
console.log(test2);

const test3=testProb4.title(t3);
console.log(test3);