function strtonum(ipaddr){
    if(ipaddr.length != 15)
        return "Invalid IP address"
    else{
        for(let i=0;i<ipaddr.length;i++){
            let num=[];
            let c=ipaddr.charCodeAt(i)
            if(c!=46 && (c<48 || c>57)){
                return num;
            }
               
        }
        let numList=ipaddr.split('.').map(Number);
        //console.log(typeof(numList[0]))
        return numList;
    }
}

module.exports.strconvert=strtonum;

/*console.log(strtonum('111.189.213.112'));
console.log(strtonum('111.18/.213.112'));
console.log(strtonum('abc.def.213.112'));
console.log(strtonum('111.189.23.1'));*/